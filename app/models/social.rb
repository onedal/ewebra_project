class Social < ActiveRecord::Base

  belongs_to :user

  def self.add_vk_info(options={})
    social = Social.find_by_uid(options[:uid])
    if social
      social.update_attributes(
          vk_token: options[:token],
          vk_email: options[:email],
          id_vk: options[:id]
      )
      social.save
    end
  end


  def self.add_tw_info(options={})
    social = Social.find_by_uid(options[:uid])
    if social
      social.update_attributes(
          twitter_token: options[:token],
          twitter_secret: options[:secret],
          id_twitter: options[:id]
      )
      social.save
    end
  end




end
