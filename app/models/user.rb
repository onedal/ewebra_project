class User < ActiveRecord::Base

  has_one :user_role
  has_one :social

  validates :email,    :uniqueness => true,
            :allow_blank => true


  has_secure_password



end
