class SocialsController < ApplicationController



  def vk
    session[:state] = Digest::MD5.hexdigest(rand.to_s)
    redirect_to VkontakteApi.authorization_url(scope:
                                                   [:notify,
                                                    :stats,
                                                    :friends,
                                                    :email,
                                                    :offline,
                                                    :photos],
                                               state: session[:state])
  end

  def vk_callback
    redirect_to login_url, alert: 'Ошибка авторизации' if params[:state] != session[:state]

    session[:state] = nil

    client = VkontakteApi.authorize(code: params['code'])

    Social.add_vk_info(token: client.token ,
                       email: client.email ,
                       id: client.user_id,
                       uid: current_user.id
    )

    redirect_to current_user

  end


  def twitter
    consumer = OAuth::Consumer.new(Rails.application.secrets.twitter_key,
                                   Rails.application.secrets.twitter_secret, {:site => 'https://api.twitter.com'})

    request_token = consumer.get_request_token({ oauth_callback: Rails.application.secrets.twitter_callback, state: session[:state] })
    session[:request_token] = request_token.token
    session[:request_token_secret] = request_token.secret

    redirect_to request_token.authorize_url

  end

  ################
  #
  #  Logging in with Twitter (callback handler)
  #
  #  @param   /config/secrets.yml
  #  @param   Array   params
  #  @return  Object  User
  #
  ###############
  def twitter_callback


    str_error = 'There was an error trying to login to your Facebook account, please try again.'

    begin
      oauth_consumer = OAuth::Consumer.new(Rails.application.secrets.twitter_key,
                                           Rails.application.secrets.twitter_secret, {:site => 'https://api.twitter.com'})

      request_token = OAuth::RequestToken.new(oauth_consumer, session[:request_token], session[:request_token_secret])

      session[:request_token] = nil
      session[:request_token_secret] = nil

      access_token = request_token.get_access_token(:oauth_verifier => params[:oauth_verifier])

      response = access_token.get('/1.1/account/verify_credentials.json')

      tw = case response
                            when Net::HTTPSuccess then JSON.parse(response.body)
                            else
                              flash[:error] = str_error
                              redirect_to '/login'
                          end


      Social.add_tw_info(token: access_token.token ,
                         secret: access_token.secret,
                         id: tw['id'],
                         uid: current_user.id
      )


          #tw['screen_name']

      redirect_to current_user
    rescue OAuth::Unauthorized
      flash[:error] = str_error
      redirect_to root_path
    end


  end


end

