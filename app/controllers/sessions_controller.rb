class SessionsController < ApplicationController
  before_filter :auth?, only: :destroy

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:uid] = user.id
      redirect_to user
    else
      flash[:notice] = 'Email or password not correct'
      redirect_to '/login'
    end
  end

  def destroy
    session[:uid] = nil
    redirect_to '/login'
  end

  def index
  end

end
