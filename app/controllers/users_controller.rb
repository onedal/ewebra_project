class UsersController < ApplicationController
  before_filter :auth?, only: :show
  def new
    @user = User.new
  end

  def create
    user = User.new(user_params)
    if user.save
      Social.create(uid: user.id)

      session[:uid] = user.id
      redirect_to user
    else
      flash[:notice] = user.errors.messages
      redirect_to '/signup'
    end
  end

  private

  def user_params
    params.require(:user).permit(:user_name, :email, :password, :password_confirmation)
  end

end
