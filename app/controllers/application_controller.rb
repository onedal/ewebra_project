class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  helper_method :vk_auth?
  helper_method :tw_auth?
end



def current_user
  @current_user ||= User.find(session[:uid]) if session[:uid]
end


def auth?
  unless current_user
    flash[:notice] = 'You need authorization, Ты понял?'
    redirect_to '/login'
  end
end

def vk_auth?
  Social.find_by_uid(current_user.id).id_vk ? true : false
end

def tw_auth?
  Social.find_by_uid(current_user.id).id_twitter ? true : false
end