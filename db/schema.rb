# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151010112916) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "projects", id: :bigserial, force: :cascade do |t|
    t.string   "name",                 null: false
    t.string   "url",                  null: false
    t.integer  "uid",        limit: 8, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "socials", id: :bigserial, force: :cascade do |t|
    t.integer  "uid",            limit: 8, null: false
    t.string   "id_vk"
    t.string   "id_twitter"
    t.string   "id_gplus"
    t.string   "id_od"
    t.string   "id_lj"
    t.string   "twitter_token"
    t.string   "twitter_secret"
    t.string   "vk_token"
    t.string   "vk_email"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :bigserial, force: :cascade do |t|
    t.string   "user_name",                 default: "",   null: false
    t.string   "email",                                    null: false
    t.string   "password_digest"
    t.boolean  "status",                    default: true, null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "role",            limit: 2
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "projects", "users", column: "uid", name: "projects_uid_fkey", on_update: :cascade, on_delete: :nullify
  add_foreign_key "socials", "users", column: "uid", name: "socials_uid_fkey", on_update: :cascade, on_delete: :nullify
  add_foreign_key "users", "user_roles", column: "role", name: "users_role_fkey", on_update: :cascade, on_delete: :nullify
end
