class CreateSocials < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.bigint :uid , null: false
      t.string :id_vk
      t.string :id_twitter
      t.string :id_gplus
      t.string :id_od
      t.string :id_lj
      t.string :twitter_token
      t.string :twitter_secret
      t.string :vk_token
      t.string :vk_secret
      t.timestamps null: false
    end
    execute '
    ALTER TABLE socials ADD FOREIGN KEY (uid) REFERENCES users (id) ON DELETE
      SET NULL ON UPDATE CASCADE'
  end
end
