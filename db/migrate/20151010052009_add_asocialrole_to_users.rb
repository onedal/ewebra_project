class AddAsocialroleToUsers < ActiveRecord::Migration
  def change
  end

  execute '
    ALTER TABLE users ADD FOREIGN KEY (role) REFERENCES user_roles (id) ON DELETE
      SET NULL ON UPDATE CASCADE'
end
