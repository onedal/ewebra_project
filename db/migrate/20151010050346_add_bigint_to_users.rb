class AddBigintToUsers < ActiveRecord::Migration
  def change
    change_column :users, :id, :bigint
    change_column :user_roles, :id, :smallint
    change_column :socials, :id, :bigint
    change_column :projects, :id, :bigint
  end

end
