class ChangeVkSecretToSocials < ActiveRecord::Migration
  def change
    rename_column :socials , :vk_secret, :vk_email
  end
end
