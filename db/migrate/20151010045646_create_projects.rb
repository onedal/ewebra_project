class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name , null: false
      t.string :url , null: false
      t.bigint :uid , null: false
      t.timestamps null: false
    end

    execute '
    ALTER TABLE projects ADD FOREIGN KEY (uid) REFERENCES users (id) ON DELETE
      SET NULL ON UPDATE CASCADE'
  end
end
