class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name, default: '', null: false
      t.string :email, null: false
      t.string :password_digest
      t.boolean :status , default: true, null: false
      t.timestamps null: false
    end
  end
end
